package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.crips.jackal.delfilholocutor.DetailActivity;
import org.crips.jackal.delfilholocutor.Galeria;
import org.crips.jackal.delfilholocutor.GalleryAdapter;
import org.crips.jackal.delfilholocutor.R;
import org.crips.jackal.delfilholocutor.RecyclerItemClickListener;

import java.util.ArrayList;


public class FragmentPagerGaleria extends Fragment {

    private RecyclerView recyclerview;
    private GalleryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Galeria> mGaleria = new ArrayList<>();

    private DatabaseReference mDatabaseRef;
    //for onDestroy()
    private ValueEventListener mValueEventListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categorias_pager, container, false);


        recyclerview = view.findViewById(R.id.recyclerview);


        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Galeria");


        mLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setAdapter(mAdapter);


        recyclerview.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {

                    @Override
                    public void onItemClick(View view, int position) {

                        Intent intent = new Intent(getActivity(), DetailActivity.class);
                        intent.putParcelableArrayListExtra("mGaleria", mGaleria);
                        intent.putExtra("pos", position);
                        startActivity(intent);

                    }
                }));

        mValueEventListener = mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mGaleria.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {

                    Galeria galeria = child.getValue(Galeria.class);
                    mGaleria.add(galeria);
                }

                mAdapter = new GalleryAdapter(getActivity(), mGaleria);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
        return view;
    }
}
