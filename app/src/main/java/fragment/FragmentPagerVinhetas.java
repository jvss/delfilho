package fragment;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.crips.jackal.delfilholocutor.Audio;
import org.crips.jackal.delfilholocutor.AudioAdapter;
import org.crips.jackal.delfilholocutor.DelPlayer;
import org.crips.jackal.delfilholocutor.R;

import java.util.ArrayList;


public class FragmentPagerVinhetas extends Fragment {

    private static final String TAG = "FragmentPagerVinhetas";

    private RecyclerView recyclerview;
    private AudioAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Audio> audioArrayList = new ArrayList<>();

    private DatabaseReference mDatabaseRef;
    //for onDestroy()
    private ValueEventListener mValueEventListener;

    private MediaPlayer mCurrentPlayer;
    private DelPlayer player;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        // não precisar ficar recriando esse objeto para cada click
        // só uma instancia basta
        player = new DelPlayer();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categorias_pager, container, false);

        recyclerview = view.findViewById(R.id.recyclerview);

        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Faixas");

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setAdapter(mAdapter);

        mValueEventListener = mDatabaseRef
                .orderByChild("categoria")
                .equalTo("Vinheta")
                .addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        audioArrayList.clear();
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            Audio audio = data.getValue(Audio.class);
                            if (audio != null) {
                                audio.setKey(data.getKey());
                                audioArrayList.add(audio);
                            }
                        }

                        if (mAdapter == null)
                            mAdapter = new AudioAdapter(getContext(), audioArrayList);
                        else
                            mAdapter.setDataset(audioArrayList);

                        mAdapter.setOnAudioPlayClickListener(new AudioAdapter.OnAudioPlayClickListener() {
                            @Override
                            public void onAudioPlay(Audio audio) {
                                player.play(getActivity(), audio);
                            }
                        });
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mCurrentPlayer != null)
            if (mCurrentPlayer.isPlaying())
                mCurrentPlayer.stop();

    }
}
