package fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.crips.jackal.delfilholocutor.Audio;
import org.crips.jackal.delfilholocutor.AudioAdapter;
import org.crips.jackal.delfilholocutor.R;

import java.util.ArrayList;


public class FragmentPagerPodcast extends Fragment {

    private RecyclerView recyclerview;
    private AudioAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Audio> audioArrayList = new ArrayList<>();

    private DatabaseReference mDatabaseRef;
    //for onDestroy()
    private ValueEventListener mValueEventListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categorias_pager, container, false);


        recyclerview = view.findViewById(R.id.recyclerview);


        mDatabaseRef = FirebaseDatabase.getInstance().getReference("Faixas");

        audioArrayList.add(new Audio("John Petrucci", "Glassgow Kiss", "Categoria 1", "http://192.168.0.100/mix/song.mp3"));
        audioArrayList.add(new Audio("Korn", "Coming Undone", "Categoria 2", "http://192.168.0.100/mix/song2.mp3"));
        audioArrayList.add(new Audio("Korn", "Liar", "Categoria 3", "http://192.168.0.100/mix/song3.mp3"));
        audioArrayList.add(new Audio("Hellish War", "Son of the King", "Categoria 4", "http://192.168.0.100/mix/song4.mp3"));

        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(mLayoutManager);
        mAdapter = new AudioAdapter(getActivity(), audioArrayList);
        mAdapter.setOnAudioPlayClickListener(new AudioAdapter.OnAudioPlayClickListener() {
            @Override
            public void onAudioPlay(Audio audio) {
                Toast.makeText(getActivity(), audio.getTitulo(), Toast.LENGTH_SHORT).show();
            }
        });
        recyclerview.setAdapter(mAdapter);



/*

        mValueEventListener = mDatabaseRef.orderByChild("categoria")
                .equalTo("Podcast").addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        audioArrayList.clear();
                        for (DataSnapshot data : dataSnapshot.getChildren()) {
                            Audio audio = data.getValue(Audio.class);
                            audio.setKey(data.getKey());
                            audioArrayList.add(audio);
                        }
                        mAdapter = new AudioAdapter(getActivity(), audioArrayList);
                        mAdapter.setOnAudioPlayClickListener(new AudioAdapter.OnAudioPlayClickListener() {
                            @Override
                            public void onAudioPlay(int position) {
                                Audio audioAtual = audioArrayList.get(position);
                                Toast.makeText(getActivity(), audioAtual.getTitulo(), Toast.LENGTH_SHORT).show();
                            }
                        });
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getActivity(), databaseError.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
*/


        return view;
    }
}
