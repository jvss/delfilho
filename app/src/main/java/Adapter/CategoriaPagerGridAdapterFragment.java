package Adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import fragment.FragmentPagerChamadas;
import fragment.FragmentPagerComerciais;
import fragment.FragmentPagerGaleria;
import fragment.FragmentPagerMensagens;
import fragment.FragmentPagerPodcast;
import fragment.FragmentPagerSpotComerciais;
import fragment.FragmentPagerVinhetas;

public class CategoriaPagerGridAdapterFragment extends FragmentPagerAdapter {

    int mNoOfTabs;

    public CategoriaPagerGridAdapterFragment(FragmentManager fm, int NumberOfTabs) {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {

            case 0:
                FragmentPagerGaleria tab1 = new FragmentPagerGaleria();
                return tab1;
            case 1:
                FragmentPagerVinhetas tab2 = new FragmentPagerVinhetas();
                return tab2;
            case 2:
                FragmentPagerComerciais tab3 = new FragmentPagerComerciais();
                return tab3;
            case 3:
                FragmentPagerChamadas tab4 = new FragmentPagerChamadas();
                return tab4;
            case 4:
                FragmentPagerSpotComerciais tab5 = new FragmentPagerSpotComerciais();
                return tab5;
            case 5:
                FragmentPagerMensagens tab6 = new FragmentPagerMensagens();
                return tab6;
            case 6:
                FragmentPagerPodcast tab7 = new FragmentPagerPodcast();
                return tab7;


            default:
                return null;

        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;

    }
}

