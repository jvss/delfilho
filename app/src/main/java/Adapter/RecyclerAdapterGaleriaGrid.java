package Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.crips.jackal.delfilholocutor.Galeria;
import org.crips.jackal.delfilholocutor.R;

import java.util.ArrayList;
import java.util.List;


public class RecyclerAdapterGaleriaGrid extends RecyclerView.Adapter<RecyclerAdapterGaleriaGrid.MyViewHolder> {
    Context context;
    int myPos = 0;
    private List<Galeria> mGaleria;


    public RecyclerAdapterGaleriaGrid(Context context, ArrayList<Galeria> mGaleria) {
        this.mGaleria = mGaleria;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_general_card, parent, false);


        return new MyViewHolder(itemView);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Galeria galeria = mGaleria.get(position);
        Glide.with(context.getApplicationContext()).load(galeria.getUrl()).into(holder.mCapa);

        holder.mTitulo.setText("\u20B9 63,999");
        holder.mTitulo.setPaintFlags(holder.mTitulo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (position == 0 | position == 1) {
            holder.mValor.setVisibility(View.VISIBLE);

        } else {
            holder.mValor.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return mGaleria.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView mTitulo, mValor;
        ImageView mCapa;
        LinearLayout mAutor;


        public MyViewHolder(View view) {
            super(view);
/*
            mCapa = view.findViewById(R.id.capa_livro_card);
            mTitulo = view.findViewById(R.id.titulo_livro_card);
            mValor = view.findViewById(R.id.valor_livro_card);
            mAutor = view.findViewById(R.id.autor_livro_card);*/

        }

    }


}


