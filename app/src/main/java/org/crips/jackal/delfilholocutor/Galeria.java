package org.crips.jackal.delfilholocutor;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.Exclude;

/**
 * Created by Suleiman19 on 10/22/15.
 */
public class Galeria implements Parcelable {

    @Exclude
    public static final Creator<Galeria> CREATOR = new Creator<Galeria>() {
        @Exclude
        @Override
        public Galeria createFromParcel(Parcel in) {
            return new Galeria(in);
        }

        @Exclude
        @Override
        public Galeria[] newArray(int size) {
            return new Galeria[size];
        }
    };
    private String descricao;
    private String url;

    public Galeria() {
    }

    public Galeria(String descricao, String url) {
        this.descricao = descricao;
        this.url = url;
    }

    protected Galeria(Parcel in) {
        descricao = in.readString();
        url = in.readString();
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Exclude
    @Override
    public int describeContents() {
        return 0;
    }

    @Exclude
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(descricao);
        dest.writeString(url);
    }
}
