package org.crips.jackal.delfilholocutor;

import com.google.firebase.database.Exclude;

public class Audio {
    private String titulo;
    private String autor;
    private String url;
    private String categoria;
    private String key;

    public Audio() {
    }//For Firebase

    public Audio(String autor, String titulo, String categoria, String url) {
        this.autor = autor;
        this.titulo = titulo;
        this.url = url;
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Exclude
    public String getKey() {
        return key;
    }

    @Exclude
    public void setKey(String key) {
        this.key = key;
    }
}
