package org.crips.jackal.delfilholocutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;

import java.util.ArrayList;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryVH> {

    Context context;
    ArrayList<Galeria> data;

    public GalleryAdapter(Context context, ArrayList<Galeria> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public GalleryVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View gvh = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new GalleryVH(gvh);
    }

    @Override
    public void onBindViewHolder(GalleryVH holder, int position) {
        Glide.with(context).load(data.get(position).getUrl())
                .thumbnail(0.5f)
                .transition(new DrawableTransitionOptions().crossFade())
                .override(200, 200)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.mImg);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class GalleryVH extends RecyclerView.ViewHolder {
        ImageView mImg;

        public GalleryVH(View itemView) {
            super(itemView);
            mImg = itemView.findViewById(R.id.item_img);
        }

    }
}
