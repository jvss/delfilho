package org.crips.jackal.delfilholocutor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.AudioVH> {
    public OnAudioPlayClickListener mListener;
    private List<Audio> mAudioList;
    private Context mContext;

    public AudioAdapter(Context context, ArrayList<Audio> mAudioList) {
        this.mAudioList = mAudioList;
        this.mContext = context;
    }

    public class AudioVH extends RecyclerView.ViewHolder {
        private TextView mAudioTitulo;
        private TextView mAudioAutor;

        public AudioVH(@NonNull View itemView) {
            super(itemView);
            mAudioTitulo = itemView.findViewById(R.id.audio_titulo);
            mAudioAutor = itemView.findViewById(R.id.audio_autor);
        }
    }

    public void setDataset(List<Audio> dataset) {
        this.mAudioList = dataset;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AudioVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View avh = LayoutInflater.from(mContext).inflate(R.layout.item_faixa_card, parent, false);
        return new AudioVH(avh);
    }

    @Override
    public void onBindViewHolder(@NonNull AudioVH holder, final int position) {

        final Audio audio = getItem(position);

        holder.mAudioTitulo.setText(mAudioList.get(position).getTitulo());
        holder.mAudioAutor.setText(mAudioList.get(position).getAutor());

        // voce estava setando o clickListener no construtor do viewHolder
        // tem que ser aqui em onBindViewHolder, para cada item da lista
        // em vez de passar position para onAudioPlay, passe logo o objeto, já que a intenção é pegar ele lá no fragmento
        // porque se voce passar a position aqui e o seu dataset mudar lá no fragment a position lá vai ser outro item
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onAudioPlay(audio);
            }
        });
    }

    private Audio getItem(int position) {
        return mAudioList.get(position);
    }

    @Override
    public int getItemCount() {
        return mAudioList.size();
    }

    public void setOnAudioPlayClickListener(OnAudioPlayClickListener listener) {
        mListener = listener;
    }

    public interface OnAudioPlayClickListener {
        void onAudioPlay(Audio audio);
    }
}





























