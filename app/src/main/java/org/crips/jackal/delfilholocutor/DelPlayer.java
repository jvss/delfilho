package org.crips.jackal.delfilholocutor;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.widget.Toast;

public class DelPlayer {

    // media player aqui não precisa ser static
    private MediaPlayer mediaPlayer;

    // ultimo audio clicado
    private Audio mLastPlayedAudio;

    public void play(final Context context, final Audio audio) {

        // se o for o mesmo audio, pára
        if (mLastPlayedAudio != null && mLastPlayedAudio.getKey().equals(audio.getKey())) {
            stop();
            mLastPlayedAudio = null;
            return;
        }

        // para o audio anterior
        stop();

        // cria um novo player
        mediaPlayer = MediaPlayer.create(context, Uri.parse(audio.getUrl()));
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                Toast.makeText(context, audio.getTitulo(), Toast.LENGTH_SHORT).show();
                mediaPlayer.start();
                mLastPlayedAudio = audio;
            }
        });
    }

    private void stop() {
        if (mediaPlayer != null && mediaPlayer.isPlaying())
            mediaPlayer.stop();
    }
}
